from database import db_session, init_db
from models import Campanha, Rolagem
from flask import Flask, render_template, redirect, url_for
import dice

app = Flask(__name__)

@app.teardown_appcontext
def shutdown_session(exception=None):
	db_session.remove()

@app.route('/')
def mostrar_campanhas():
	resultado = Campanha.query.all()
	return render_template('index.html', resultado=resultado)

@app.route('/<campanha>')
def mostrar_rolagens(campanha):
	c = Campanha.query.filter(Campanha.nome == campanha).first()
	resultado = Rolagem.query.filter(Rolagem.campanha_id == c.id).all()
	return render_template('rolagens.html', resultado=resultado)	

@app.route('/add/<nome>')
@app.route('/add/<nome>/<sistema>')
def criar_campanha(nome, sistema=''):
	c = Campanha(nome=nome, sistema=sistema)
	db_session.add(c)
	db_session.commit()
	return redirect(url_for('mostrar_campanhas'))

@app.route('/alt/<nome>/<sistema>')
def mudar_sistema_campanha(nome, sistema):
	c = Campanha.query.filter(Campanha.nome == nome).first()
	c.sistema = sistema
	db_session.commit()
	return redirect(url_for('mostrar_campanhas'))

@app.route('/roll/<campanha>/<jogador>/<parada>')
def rolar_dados(campanha, jogador, parada):
	resultado = dice.roll(parada + 't')
	c = Campanha.query.filter(Campanha.nome == campanha).first()
	rolagem = Rolagem()
	rolagem.campanha = c
	rolagem.jogador = jogador
	rolagem.parada = parada
	rolagem.resultado = resultado
	db_session.add(rolagem)
	db_session.commit()
	return redirect(url_for('mostrar_campanhas'))	

@app.route('/init_db')
def iniciar_banco():
	init_db()
	return redirect(url_for('mostrar_campanhas'))	