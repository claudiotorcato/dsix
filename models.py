from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from database import Base

class Campanha(Base):
	__tablename__ = 'campanhas'
	id = Column(Integer, primary_key=True)
	nome = Column(String(50), unique=True)
	sistema = Column(String(50))
	rolagens = relationship('Rolagem', back_populates='campanha')

	def __init__(self, nome=None, sistema=''):
		self.nome = nome
		self.sistema = sistema

	def __repr__(self):
		return '<Campanha %r>' % (self.nome)

class Rolagem(Base):
	__tablename__ = 'rolagens'

	id = Column(Integer, primary_key = True)
	campanha_id = Column(Integer, ForeignKey('campanhas.id'))
	campanha = relationship('Campanha', back_populates='rolagens')
	jogador = Column(String(50))
	parada = Column(String(10))
	resultado = Column(Integer)